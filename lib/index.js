"use strict";

const NSMatcher = require("ns-matcher");
const winston = require.main.require("winston");

const Meta = module.parent.require("./meta");

const pluginData = require("../plugin.json");
pluginData.nbbId = pluginData.id.replace(/^nodebb-plugin-/, "");

const ERROR_MSG = "Email domain not accepted.";
const NS_OPTIONS = {
  scopeSeparatorExp: "\\.",
  scopeSeparatorNegativeExp: "[^\\.]",
  scopeSeparatorPatternExp: "\\\\."
};

/*===================================================== Exports  =====================================================*/

exports.load = load;
exports.checkOnRegister = checkOnRegister;
exports.checkOnUpdate = checkOnUpdate;
exports.admin = {navigation: registerAdminNavigation};

/*==================================================== Variables  ====================================================*/

let testFn = () => true;

/*==================================================== Functions  ====================================================*/

function load(data, cb) {
  Meta.settings.get(pluginData.nbbId, function (err, settings) {
    if (err) { return cb(err); }
    if (!settings) {
      winston.warn('[plugins/' + pluginData.nbbId + '] Settings not set or could not be retrieved!');
      return cb();
    }

    winston.info('[plugins/' + pluginData.nbbId + '] Settings loaded');
    parseSettings(settings.pattern || "");

    data.router.get('/admin/plugins/' + pluginData.nbbId, data.middleware.admin.buildHeader, render);
    data.router.get('/api/admin/plugins/' + pluginData.nbbId, render);

    cb();
  });

  function render(req, res, next) { res.render('admin/plugins/' + pluginData.nbbId, pluginData || {}); }
}

function parseSettings(pattern) {
  const matcher = new NSMatcher(pattern, NS_OPTIONS);
  testFn = matcher.test.bind(matcher);
}

function checkOnRegister(data, next) {
  if (data && data.userData && data.userData.email && !testFn(domain(data.userData.email))) {
    return next(new Error(ERROR_MSG));
  }
  return next(null, data);
}

function checkOnUpdate(data, next) {
  if (data && data.data && data.data.email && !testFn(domain(data.data.email))) { return next(new Error(ERROR_MSG)); }
  return next(null, data);
}

function domain(email) { return email.substring(email.indexOf('@') + 1); }

function registerAdminNavigation(header, callback) {
  header.plugins.push({
    "route": '/plugins/' + pluginData.nbbId,
    "icon": pluginData.faIcon,
    "name": pluginData.name
  });

  callback(null, header);
}
